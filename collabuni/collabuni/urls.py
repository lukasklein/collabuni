from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'collabuni.views.home', name='home'),
    # url(r'^collabuni/', include('collabuni.foo.urls')),

    url(r'^', include('pages.urls')),
    url(r'^courses/', include('courses.urls')),
    url(r'^api/', include('api.urls')),
    url(r'^upload/', 'courses.views.upload'),
    url(r'^download/(?P<id>\d+)/$', 'courses.views.download'),
    url(r'^download/(?P<id>\d+)/raw/$', 'courses.views.download_raw'),
    url(r'^account/', include('account.urls')),
    url(r'^join/', 'account.views.join'),
    url(r'^thanks/', 'account.views.thanks'),
    url(r'^accounts/login/', 'django.contrib.auth.views.login', {'template_name': 'login.html', 'extra_context': {'headline': 'Login'}}),
    url(r'^webdav/', include('collabuni_webdav.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
