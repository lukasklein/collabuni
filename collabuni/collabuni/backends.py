import hashlib
from django.contrib.auth.models import User
from collabuni.models import UserNotification


class EmailAuthBackend(object):
    """
    Email Authentication Backend

    Allows a user to sign in using an email/password pair rather than
    a username/password pair.
    """

    def authenticate(self, username=None, password=None):
        """ Authenticate a user based on email address as the user name. """
        try:
            user = User.objects.get(email=username)
            if user.check_password(password):
                #profile = user.get_profile()
                #if not profile.webdav_digest or True:
                #    digest = hashlib.md5()
                #    digest.update('%s:Collabuni:%s' % (username, password))
                #    digest = digest.hexdigest()
                #    profile.webdav_digest = digest
                #    profile.save()
                return user
        except User.DoesNotExist:
            return None

    def get_user(self, user_id):
        """ Get a User object from the user_id. """
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None


def context_user(request):
    if hasattr(request, 'user'):
        if request.user.is_authenticated():
            notifications_all = UserNotification.objects.filter(user=request.user).order_by('-created')
            notifications = []
            for notification in notifications_all:
                if notification.read == False or len(notifications) < 5:
                    notifications.append(notification)
            return {
                'path': request.path,
                'user': request.user,
                'notifications': notifications,
                'notification_unread_count': len(UserNotification.objects.filter(user=request.user, read=False)),
            }
    return {
        'path': request.path,
    }
