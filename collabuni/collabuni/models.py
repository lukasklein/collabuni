from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.utils.translation import ugettext as _
from unis.models import Uni


class UserProfile(models.Model):
    user = models.OneToOneField(User)

    uni = models.ForeignKey(Uni, null=True, blank=True)
    uni_mail = models.EmailField()

    def __unicode__(self):
        return "%s - %s" % (self.user.email, self.uni.name)


def create_user_profile(sender, instance, created, **kwargs):
    if created:
        try:
            uni = Uni.objects.get(mails__domain=instance.email.split('@')[1])
        except ObjectDoesNotExist:
            uni = None
        UserProfile.objects.create(user=instance, uni=uni, uni_mail=instance.email)

post_save.connect(create_user_profile, sender=User)


class UserNotification(models.Model):
    user = models.ForeignKey(User, related_name='notifications')
    created = models.DateTimeField(auto_now_add=True)
    message = models.TextField()
    read = models.BooleanField(default=False)
    link = models.CharField(max_length=255)
    type = models.CharField(max_length=255)
    done = models.BooleanField(default=False)


def notify_user(sender, instance, created, **kwargs):
    if created:
        subject = _('New notification on Collabuni')
        message = _('%s\n\nClick here to go to Collabuni: http://collabuni.com' % instance.message)
        m_from = 'collabuni@productgang.com'
        to = instance.user.email
        send_mail(subject, message, m_from, [to], fail_silently=True)

post_save.connect(notify_user, sender=UserNotification)
