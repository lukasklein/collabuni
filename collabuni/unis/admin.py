from unis.models import UniMail, Uni
from django.contrib import admin

admin.site.register(UniMail)
admin.site.register(Uni)
