from django.db import models


class UniMail(models.Model):
    domain = models.CharField(max_length=255)

    def __unicode__(self):
        return self.domain


class Uni(models.Model):
    name = models.CharField(max_length=255)
    COUNTRIES_AVAILABLE = (
        ('DE', 'Deutschland'),
    )
    country = models.CharField(max_length=2, choices=COUNTRIES_AVAILABLE)
    mails = models.ManyToManyField(UniMail, related_name='universities')

    def __unicode__(self):
        return self.name
