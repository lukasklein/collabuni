from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.template import RequestContext


def index(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('courses.views.index'))
    else:
        return render_to_response('landing.html', {
            'outercontent': True,
        }, context_instance=RequestContext(request))
