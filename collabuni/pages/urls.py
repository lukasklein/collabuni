from django.conf.urls import patterns, url
from django.views.generic.simple import direct_to_template

urlpatterns = patterns('',
    url(r'^privacy/$', direct_to_template, {'template': 'privacy.html', 'extra_context': {'headline': 'Privacy Policy'}}),
    url(r'^$', 'pages.views.index'),
)
