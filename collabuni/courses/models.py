from uuid import uuid4
import os
from django.db import models
from django.contrib.auth.models import User
from django.contrib import messages
from unis.models import Uni


class Course(models.Model):
    name = models.CharField(max_length=255)
    uni = models.ForeignKey(Uni)
    description = models.TextField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    last_changed = models.DateTimeField(auto_now=True)
    admins = models.ManyToManyField(User, related_name='courses_admin')
    members = models.ManyToManyField(User, related_name='courses_member')
    pending = models.ManyToManyField(User, related_name='courses_pending', blank=True)

    def __unicode__(self):
        return "%s (%s)" % (self.name, self.uni)

    def get_latest_files(self, count=5):
        files = File.objects.filter(course=self).order_by('-uploaded')[:count]
        return files

    def clean(self):
        from django.core.exceptions import ValidationError
        if '/' in self.name or '\\' in self.name:
            raise ValidationError('The course name must not contain slashes!')

    @models.permalink
    def get_absolute_url(self):
        return ('courses.views.view', {'id': str(self.id)})


class Folder(models.Model):
    name = models.CharField(max_length=255)
    creator = models.ForeignKey(User, related_name='folders')
    parent = models.ForeignKey('self', blank=True, null=True)
    course = models.ForeignKey(Course)

    def __unicode__(self):
        return "%s (by %s (%s at %s))" % (self.name, self.creator.email, self.course, self.course.uni)

    def save(self):
        if Folder.objects.filter(parent=self.parent, name=self.name, course=self.course):
            return False
        else:
            super(Folder, self).save()
            return True

    def lastchange(self):
        latestfile = File.objects.filter(parent=self, course=self.course).order_by('-uploaded')
        if latestfile:
            return latestfile[0].uploaded
        else:
            return ''

    def parents(self):
        parent = self
        parents = []
        while parent:
            parents.insert(0, parent)
            parent = parent.parent
        return parents

    @models.permalink
    def get_absolute_url(self):
        return ('folderview', {'id': str(self.course.id), 'folder': str(self.id)})


def get_random_filename(instance, filename):
    instance.name = filename
    ext = os.path.splitext(filename)[1]
    filename = "%s%s" % (str(uuid4()), ext)
    return filename


class File(models.Model):
    file = models.FileField(upload_to=get_random_filename)
    name = models.CharField(max_length=255, blank=True)
    parent = models.ForeignKey(Folder, blank=True, null=True)
    owner = models.ForeignKey(User, related_name='files')
    uploaded = models.DateTimeField(auto_now_add=True)
    course = models.ForeignKey(Course)
    directdownload = models.NullBooleanField(default=False, null=True)

    def __init__(self, *args, **kwargs):
        super(File, self).__init__(*args, **kwargs)
        self.known_extensions = {
            'ppt': 'presentation',
            'key': 'presentation',
            'doc': 'word',
            'docx': 'word',
            'pages': 'word',
            'numbers': 'spreadsheet',
            'py': 'code',
            'java': 'code',
            'html': 'code',
            'js': 'code',
            'htm': 'code',
            'png': 'image',
            'jpg': 'image',
            'jpeg': 'image',
            'gif': 'image',
            'pdf': 'pdfdocument',
            'zip': 'archive',
            'tar': 'archive',
            'tar.gz': 'archive',
            'tar.bz2': 'archive',
            'txt': 'text',
        }

    def __unicode__(self):
        return "%s (by %s (%s at %s))" % (self.name, self.owner.email, self.course, self.course.uni)

    @models.permalink
    def get_absolute_url(self):
        return ('courses.views.download', {'id': str(self.id)})

    def delete(self):
        os.rename(self.file.path, self.file.path + '_delete')
        super(File, self).delete()

    def save(self):
        file = self.file
        if file:
            if file.size > 5 * 1024 * 1024:
                return False
            super(File, self).save()
            return True
        else:
            raise False

    def filetype(self):
        extension = os.path.splitext(self.name)[1][1:]
        return self.known_extensions.get(extension, 'unknown')

    def extension(self):
        extension = os.path.splitext(self.name)[1][1:]
        extension = self.known_extensions.get(extension, 'unknown')
        return "img/file-extensions/%s.png" % extension

    def get_comment_count(self):
        comments = FileComment.objects.filter(file=self)
        return len(comments)


class FileComment(models.Model):
    file = models.ForeignKey(File, related_name='comments')
    user = models.ForeignKey(User, related_name='comments')
    posted = models.DateTimeField(auto_now_add=True)
    changed = models.DateTimeField(auto_now=True)
    comment = models.TextField()


class UserFolderChange(models.Model):
    user = models.ForeignKey(User, related_name='folderchanges')
    folder = models.ForeignKey(Folder, related_name='folderchanges', null=True)


class UserFileChange(models.Model):
    user = models.ForeignKey(User, related_name='filechanges')
    file = models.ForeignKey(File, related_name='filechanges')
