from django import forms
from django.utils.translation import ugettext as _


class NewCourseForm(forms.Form):
    name = forms.CharField(max_length=255)
    description = forms.CharField(widget=forms.Textarea, required=False)

    def clean_name(self):
        name = self.cleaned_data.get('name')
        if '/' in name or '\\' in name:
            raise forms.ValidationError(_("The course name must not contain any slashes!"))
        return name


class FileUploadForm(forms.Form):
    file = forms.FileField()
    parent = forms.CharField(widget=forms.HiddenInput)
    course = forms.CharField(widget=forms.HiddenInput)
