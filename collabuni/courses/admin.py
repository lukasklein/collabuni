from courses.models import Course, Folder, File, FileComment
from django.contrib import admin

admin.site.register(Course)
admin.site.register(Folder)
admin.site.register(File)
admin.site.register(FileComment)
