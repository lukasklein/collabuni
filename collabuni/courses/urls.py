from django.conf.urls import patterns, url


urlpatterns = patterns('',
    url(r'^$', 'courses.views.index'),
    url(r'^create/$', 'courses.views.create'),
    url(r'^view/(?P<id>\d+)/(?P<folder>\d+)/$', 'courses.views.view', name='folderview'),
    url(r'^view/(?P<id>\d+)/$', 'courses.views.view'),
    url(r'^editfile/(?P<id>\d+)/$', 'courses.views.editfile'),
    url(r'^requests/(?P<id>\d+)/$', 'courses.views.requests'),
    url(r'^requests/(?P<id>\d+)/(?P<user>\d+)/$', 'courses.views.requests'),
)
