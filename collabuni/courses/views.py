import mimetypes
import os
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext
from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.contrib.auth.models import User
from courses.models import Course, File, Folder, UserFileChange, UserFolderChange
from courses.forms import NewCourseForm, FileUploadForm
from collabuni.models import UserNotification


@login_required
def index(request):
    courses = Course.objects.filter(members=request.user)

    return render_to_response('index.html', {
        'headline': [_('My Courses'), reverse('courses.views.index')],
        'subheadline': _('Courses overview'),
        'courses': courses,
    }, context_instance=RequestContext(request))


@login_required
def create(request):
    tab = ''
    if request.method == 'POST':
        form = NewCourseForm(request.POST)
        tab = 'create'
        if form.is_valid():
            name = form.cleaned_data['name']
            description = form.cleaned_data['description']
            course = Course()
            course.name = name
            course.description = description
            course.uni = request.user.get_profile().uni
            course.save()
            course.admins.add(request.user)
            course.members.add(request.user)

            return HttpResponseRedirect('/courses/')
    else:
        form = NewCourseForm()

    courses = Course.objects.filter(members=request.user)

    courses_available = Course.objects.filter(uni=request.user.get_profile().uni)

    return render_to_response('create.html', {
        'headline': [_('My Courses'), reverse('courses.views.index')],
        'subheadline': _('Add Course'),
        'form': form,
        'courses': courses,
        'courses_available': courses_available,
        'tab': tab,
    }, context_instance=RequestContext(request))


@login_required
def view(request, id, folder=None):
    course = get_object_or_404(Course, members=request.user, id=id)

    if folder:
        folder = get_object_or_404(Folder, id=folder, course__members=request.user, course=course)

    courses = Course.objects.filter(members=request.user)

    files = File.objects.filter(course=course, parent=folder)
    folders = Folder.objects.filter(course=course, parent=folder)

    folder_id = None
    if folder:
        folder_id = folder.id
    fileform = FileUploadForm(
        initial={
            'parent': folder_id,
            'course': course.id
        }
    )

    if request.GET.get('notify'):
        nfile = get_object_or_404(File, id=request.GET.get('file'), course__members=request.user)
        ufc = UserFileChange()
        ufc.user = request.user
        ufc.file = nfile
        ufc.save()

        notifications = UserNotification.objects.filter(user=request.user, type__startswith='filecomment_' + str(nfile.id) + '_')
        for notification in notifications:
            notification.read = True
            notification.save()

    ufic_ = UserFileChange.objects.filter(user=request.user, file__parent=folder)
    ufic = [ufc.file for ufc in ufic_]
    ufic_.delete()

    ufoc_ = UserFolderChange.objects.filter(user=request.user, folder__parent=folder)
    ufoc = [ufc.folder for ufc in ufoc_]
    ufoc_.delete()

    return render_to_response('view.html', {
        'headline': [_('My Courses'), reverse('courses.views.index')],
        'subheadline': course.name,
        'courses': courses,
        'course': course,
        'files': files,
        'folders': folders,
        'folder': folder,
        'folder_name': folder.name if folder else '',
        'fileform': fileform,
        'ufic': ufic,
        'ufoc': ufoc,
    }, context_instance=RequestContext(request))


@login_required
def upload(request):
    if request.method == 'POST':
        course = get_object_or_404(Course, id=request.POST['course'], members=request.user)
        folder = None
        if request.POST['parent']:
            folder = get_object_or_404(Folder, course=course, id=request.POST['parent'])

        form = FileUploadForm(request.POST, request.FILES)
        if form.is_valid:
            file = File()
            file.file = request.FILES['file']
            file.parent = folder
            file.course = course
            file.owner = request.user
            if file.save():
                messages.success(request, _('You have successfully uploaded the file %s.' % file.name))

                for member in course.members.all():
                    ufc = UserFileChange()
                    ufc.user = request.user
                    ufc.file = file
                    ufc.save()
                    UserFolderChange.objects.get_or_create(user=request.user, folder=folder)

            else:
                messages.error(request, _('The file is too big (max filesize: 5mb).'))

            args = (course.id,)
            if folder:
                args = (course.id, folder.id,)
            return HttpResponseRedirect(reverse('courses.views.view', args=args))
        else:
            return HttpResponse(status=500)
    else:
        return HttpResponse(status=500)


@login_required
def editfile(request, id):
    file = get_object_or_404(File, id=id, course__members=request.user)
    courses = Course.objects.filter(members=request.user)

    return render_to_response('editfile.html', {
        'headline': [_('My Courses'), reverse('courses.views.index')],
        'subheadline': _('Edit file - %s' % file.name),
        'courses': courses,
        'file': file,
    }, context_instance=RequestContext(request))


@login_required
def requests(request, id, user=None):
    course = get_object_or_404(Course, id=id, admins=request.user)
    if user:
        if request.COOKIES['csrftoken'] != request.GET.get('token'):
            return HttpResponse(status=403)

        user = get_object_or_404(User, id=user)
        course = get_object_or_404(Course, id=id, admins=request.user, pending=user)
        course.pending.remove(user)
        course.members.add(user)
        course.save()

        messages.success(request, _('You have successfully added %s to %s.' % (user.email, course.name)))

        notifications = UserNotification.objects.filter(type='courserequest_' + str(course.id) + '_' + str(user.id))
        for notification in notifications:
            notification.done = True
            notification.save()

        return HttpResponseRedirect(reverse('courses.views.view', args=(course.id,)))

    notifications = UserNotification.objects.filter(user=request.user, type__startswith='courserequest_' + str(course.id) + '_')
    for notification in notifications:
        notification.read = True
        notification.save()

    courses = Course.objects.filter(members=request.user)

    return render_to_response('requests.html', {
        'headline': [_('My Courses'), reverse('courses.views.index')],
        'subheadline': [course.name, reverse('courses.views.view', args=(course.id,))],
        'course': course,
        'courses': courses,
    }, context_instance=RequestContext(request))


def download_raw(request, id):
    file = get_object_or_404(File, id=id, directdownload=True)
    fpath = file.file.path
    ext = os.path.splitext(file.name)[1]

    mimetypes.init()
    bytes = os.path.getsize(fpath)
    response = HttpResponse(open(fpath, 'r').read(), content_type=mimetypes.types_map[ext])
    response['Content-Length'] = bytes

    return response

@login_required
def download(request, id):
    file = get_object_or_404(File, id=id, course__members=request.user)
    fpath = file.file.path
    ext = os.path.splitext(file.name)[1]

    mimetypes.init()
    bytes = os.path.getsize(fpath)
    response = HttpResponse(open(fpath, 'r').read(), content_type=mimetypes.types_map[ext])
    response['Content-Length'] = bytes
    response['Content-Disposition'] = "attachment; filename=%s" % file.name

    return response
