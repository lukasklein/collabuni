import os, datetime, time, urllib2, tempfile, logging
from django.core.files import File as DjangoFile
from django_digest.decorators import httpdigest
from django.views.decorators.csrf import csrf_exempt
from collabuni_webdav import DavServer, DavResource, url_join, HttpNotAllowed, HttpResponsePreconditionFailed, safe_join
from courses.models import Course, Folder, File, get_random_filename



@httpdigest
@csrf_exempt  # Make PROPFIND work, don't ask...
def export(request, path):
    return CollabuniDavServer(request, path, resource_class=CollabuniDavResource).get_response()


class CollabuniDavServer(DavServer):
    def get_access(self, path):
        resource = self.get_resource(path=path)
        if resource.is_overview:  # Course overview
            return self.acl_class(list=True, read=True, all=False)
        elif type(resource.instance) == Folder:
            relocate = False
            if resource.instance.creator == self.request.user:
                relocate = True
            return self.acl_class(list=True, read=True, create=True, relocate=relocate, all=False)
        elif type(resource.instance) == File:
            relocate = delete = write = False
            if resource.instance.owner == self.request.user:
                relocate = delete = write = True
            return self.acl_class(list=True, read=True, create=True, relocate=relocate, write=write, delete=delete, all=False)
        else:
            return self.acl_class(list=True, read=True, write=True, create=True, all=False)
        return self.acl_class(list=True, read=True, all=False)


class CollabuniDavResource(DavResource):
    def __init__(self, server, path):
        self.server = server
        # Trailing / messes with dirname and basename.
        while path.endswith('/'):
            path = path[:-1]
        self.path = urllib2.unquote(path)

        self.is_overview = True
        self.instance = None
        self.parent = None

        self.logger = logging.getLogger(__name__)

        self._exists = True

        if self.path:
            self.is_overview = False
            slices = self.path.split('/')

            #  Try to get the course
            course_name = slices[0]
            try:
                self.course = Course.objects.get(name=course_name, members=self.server.request.user)
                if len(slices) == 1:  # This is a course
                    self.instance = self.course
            except Course.DoesNotExist:
                self.logger.error('Could not get course %s' % course_name)
                raise HttpNotAllowed
            else:
                for slice_name in slices[1:]:
                    try:  # Maybe this is a folder...
                        self.instance = Folder.objects.get(name=slice_name, course=self.course, parent=self.parent)
                        self.parent = self.instance
                    except Folder.DoesNotExist:  # Or even a file...
                        try:
                            self.instance = File.objects.get(name=slice_name, course=self.course, parent=self.parent)
                        except File.DoesNotExist:  # Or nothing...
                            self._exists = False

    def get_children(self):
        if self.is_overview:
            for course in Course.objects.filter(members=self.server.request.user):
                yield self.__class__(self.server, os.path.join(self.path, urllib2.quote(course.name.encode('utf-8'))))
        elif type(self.instance) in (Course, Folder):
            for folder in Folder.objects.filter(course=self.course, parent=self.parent):
                yield self.__class__(self.server, os.path.join(self.path, urllib2.quote(folder.name.encode('utf-8'))))
            for ffile in File.objects.filter(course=self.course, parent=self.parent):
                yield self.__class__(self.server, os.path.join(self.path, urllib2.quote(ffile.name.encode('utf-8'))))

    def exists(self):
        return self._exists

    def isdir(self):
        if type(self.instance) in (Course, Folder):
            return True
        else:
            return False

    def isfile(self):
        return type(self.instance) == File

    def get_parent(self):
        return self.__class__(self.server, urllib2.quote('/'.join(self.path.split('/')[:-1])))

    def get_name(self):
        if type(self.instance) in [Course, File, Folder]:
            return self.instance.name
        else:
            return self.path.split('/')[-1]

    def get_ctime_stamp(self):
        return 1

    def get_ctime(self):
        return datetime.datetime.fromtimestamp(self.get_ctime_stamp())

    def get_size(self):
        if type(self.instance) == File:
            return os.path.getsize(self.instance.file.path)
        return 1

    def get_mtime_stamp(self):
        if type(self.instance) == Course:
            latest_files = self.instance.get_latest_files()
            if latest_files:
                return int(time.mktime(latest_files[0].uploaded.timetuple()))
            else:
                return 0

    def get_mtime(self):
        return datetime.datetime.fromtimestamp(self.get_mtime_stamp())

    def get_url(self):
        return url_join(self.server.request.get_base_url(), self.path)

    def get_etag(self):
        return self.instance.id if self.instance else 0

    def get_abs_path(self):
        return self.get_path()

    def open(self, mode):
        if self.instance == None:  # Create file
            self.instance = File()
            self.instance.parent = self.parent
            self.instance.owner = self.server.request.user
            self.instance.course = self.course
            self.instance.name = self.path.split('/')[-1]
            self.instance.file.save(self.instance.name, DjangoFile(open(os.path.join(tempfile.gettempdir(), self.get_name()), 'w+')))
            self.instance.save()

        return file(self.instance.file.path, mode)

    def mkdir(self):
        folder = Folder()
        folder.name = self.path.split('/')[-1]
        folder.creator = self.server.request.user
        folder.parent = self.parent
        folder.course = self.course
        folder.save()

    def delete(self):
        print "deleting %s" % self.instance.name
        self.instance.delete()

    def move(self, destination):
        if destination.exists():
            destination.delete()
        if self.isdir():
            destination.mkdir()
            for child in self.get_children():
                child.move(self.__class__(self.server, os.path.join(destination.path, child.get_name())))
            self.delete()
        else:
            if type(self.instance) in (Folder, File):
                self.instance.name = destination.path.split('/')[-1]
                self.instance.parent = destination.parent
                self.instance.save()

    def copy(self, destination, depth=0):
        if self.isdir():
            if destination.isfile():
                destination.delete()
            if not destination.isdir():
                destination.mkdir()

            if depth != 0:
                for child in self.get_children():
                    child.copy(self.__class__(self.server, os.path.join(destination.path, child.get_name())), depth=depth - 1)
        else:
            if destination.isdir():
                destination.delete()
            self.instance.name = destination.get_name()
            self.instance.parent = destination.instance.parent
            self.instance.save()
