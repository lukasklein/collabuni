from django.conf.urls import patterns, url


urlpatterns = patterns('',
    url(r'^(?P<path>.*)$', 'collabuni_webdav.views.export')
)
