import BaseHTTPServer
import urllib2
import hashlib
import os
import random
import logging

try:
    from local_settings import *
except ImportError:
    pass

WEBDAV_LOG_FILE = 'webdav.log'
logging.basicConfig(filename=WEBDAV_LOG_FILE, level=logging.ERROR)

os.environ['PYTHONPATH'] = PROJECTPATH
os.environ['DJANGO_SETTINGS_MODULE'] = 'collabuni.settings'

from django.contrib.auth.models import User
from courses.models import Course, Folder, File
from unis.models import Uni

response_container = """<?xml version="1.0" encoding="utf-8" ?>
<D:multistatus xmlns:D="DAV:">
 %s
</D:multistatus>"""

collection_response = """<D:response>
      <D:href>%s/%s/</D:href>
      <D:propstat>
           <D:prop xmlns:R="http://www.foo.bar/boxschema/">
                <R:bigbox>
                     <R:BoxType>Box type A</R:BoxType>
                </R:bigbox>
                <R:author>
                     <R:Name>Collabuni</R:Name>
                </R:author>
                <D:creationdate>
                     1997-12-01T17:42:21-08:00
                </D:creationdate>
                <D:displayname>
                     %s
                </D:displayname>
                <D:resourcetype><D:collection/></D:resourcetype>
                <D:supportedlock>
                     <D:lockentry>
                          <D:lockscope><D:exclusive/></D:lockscope>
                          <D:locktype><D:write/></D:locktype>
                     </D:lockentry>
                     <D:lockentry>
                          <D:lockscope><D:shared/></D:lockscope>
                          <D:locktype><D:write/></D:locktype>
                     </D:lockentry>
                </D:supportedlock>
           </D:prop>
           <D:status>HTTP/1.1 200 OK</D:status>
      </D:propstat>
 </D:response>
 """


file_response = """<D:response>
  <D:href>%s/%s</D:href>
  <D:propstat>
       <D:prop xmlns:R="http://www.foo.bar/boxschema/">
            <R:bigbox>
                 <R:BoxType>Box type B</R:BoxType>
            </R:bigbox>
            <D:creationdate>
                 1997-12-01T18:27:21-08:00
            </D:creationdate>
            <D:displayname>
                 Example HTML resource
            </D:displayname>
            <D:getcontentlength>
                 4525
            </D:getcontentlength>
            <D:getcontenttype>
                 text/html
            </D:getcontenttype>
            <D:getetag>
                 zzyzx
            </D:getetag>
            <D:getlastmodified>
                 Monday, 12-Jan-98 09:25:56 GMT
            </D:getlastmodified>
            <D:resourcetype/>
            <D:supportedlock>
                 <D:lockentry>
                      <D:lockscope><D:exclusive/></D:lockscope>
                      <D:locktype><D:write/></D:locktype>
                 </D:lockentry>
                 <D:lockentry>
                      <D:lockscope><D:shared/></D:lockscope>
                      <D:locktype><D:write/></D:locktype>
                 </D:lockentry>
            </D:supportedlock>
       </D:prop>
       <D:status>HTTP/1.1 200 OK</D:status>
  </D:propstat>
</D:response>"""


class WebDav(BaseHTTPServer.BaseHTTPRequestHandler):
    def do_OPTIONS(self):
        self.send_response(200)
        self.send_header('Allow', 'OPTIONS,GET,HEAD,POST,DELETE,TRACE,PROPFIND,PROPPATCH,COPY,MOVE,LOCK,UNLOCK')
        self.send_header('DAV', '1,2')
        self.send_header('DAV', '<http://apache.org/dav/propset/fs/1>')
        self.send_header('MS-Author-Via', 'DAV')
        self.end_headers()

    def do_PROPFIND(self):
        user = self.force_auth(self)
        if user:
            logging.info('PROPFIND - %s' % (user.email))
            if self.path == '/':  # List courses
                self.send_response(207)
                self.end_headers()

                response_collections = ''
                for course in Course.objects.filter(members=user):
                    coursename = urllib2.quote(course.__unicode__().encode("utf8"))
                    response_collections += collection_response % (WEBDAV_SERVER, coursename, course.name)
                self.wfile.write(response_container % (response_collections))
            else:
                folder = self.path
                uniname = urllib2.unquote(folder.split('(')[-1].split(')')[0])
                try:
                    uni = Uni.objects.get(name=uniname)
                except Uni.DoesNotExist:
                    self.send_response(404)
                    return False

                coursename = urllib2.unquote(''.join(folder.split('(')[:-1]))[1:-1]
                try:
                    course = Course.objects.get(uni=uni, name=coursename, members=user)
                except Course.DoesNotExist:
                    self.send_response(404)
                    return False

                response_collections = self.get_folder_contents(course)

                self.send_response(207)
                self.end_headers()
                self.wfile.write((response_container % (response_collections)).encode("utf-8"))

    def get_folder_contents(self, course, parent=None):
        response_collections = ''

        tree = urllib2.quote(course.__unicode__().encode("utf8"))
        if parent:
            pparent = Folder.objects.get(id=parent, course=course)
            while pparent:
                tree = urllib2.quote(pparent.name.encode("utf-8", "replace")) + '/' + tree
                pparent = Folder.objects.get(id=pparent, course=course)
        tree += '/'

        for folder in Folder.objects.filter(course=course, parent=parent):
            foldername = urllib2.quote(folder.name.encode("utf-8", "replace"))
            response_collections += collection_response % (WEBDAV_SERVER, tree + foldername, folder.name)


        for ffile in File.objects.filter(course=course, parent=parent):
            filename = urllib2.quote(ffile.name.encode("utf-8", "replace"))
            response_collections += file_response % (WEBDAV_SERVER, tree + filename)
        print response_collections
        return response_collections

    def force_auth(self, obj):
        if not obj.headers.getheader('Authorization'):
            nonce = hashlib.md5()
            nonce.update(str(random.random()))
            nonce = nonce.hexdigest()

            opaque = hashlib.md5()
            opaque.update(str(random.random()))
            opaque = opaque.hexdigest()

            obj.send_response(401, "Unauthorized.")
            obj.send_header('WWW-Authenticate', 'Digest realm="Collabuni", qop="auth,auth-int", nonce="%s", opaque="%s"'%(nonce, opaque))
            obj.end_headers()
            return False
        else:
            authheader = obj.headers.getheader('Authorization')
            items = urllib2.parse_http_list(authheader)
            opts = urllib2.parse_keqv_list(items)

            username = opts['Digest username']
            try:
                user = User.objects.get(email=username)
                digest = user.get_profile().webdav_digest
            except User.DoesNotExist:
                obj.send_response(401, "Unauthorized.")
                return False

            HA2 = hashlib.md5()
            HA2.update('%s:%s' % (obj.command, opts['uri']))
            HA2 = HA2.hexdigest()

            response = hashlib.md5()
            response.update('%s:%s:%s:%s:%s:%s' % (digest, opts['nonce'], opts['nc'], opts['cnonce'], opts['qop'], HA2))
            response = response.hexdigest()

            if response == opts['response']:
                return user
            else:
                obj.send_response(401, "Unauthorized.")
                return False

def server_main(server_class=BaseHTTPServer.HTTPServer, handler_class=WebDav):
    server_class(('', 9231), handler_class).serve_forever()

if __name__ == "__main__":
    server_main()
