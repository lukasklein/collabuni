from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.contrib import messages
from django.utils.translation import ugettext as _
from django.utils.html import escape
from django.core.urlresolvers import reverse
from django.utils import simplejson
from courses.models import Course, Folder, File, FileComment
from collabuni.models import UserNotification


@login_required
def get_comments(request):
    mfile = get_object_or_404(File, id=request.GET['file'], course__members=request.user)
    comments = FileComment.objects.filter(file=mfile)
    json = simplejson.dumps([{'user': escape(c.user.get_full_name()) or c.user.email, 'comment': escape(c.comment), 'date_posted': c.posted.isoformat()} for c in comments])

    return HttpResponse('comments = ' + json)


@login_required
def post_comment(request):
    mfile = get_object_or_404(File, id=request.GET['file'], course__members=request.user)
    if request.method == 'POST':
        if len(request.POST.get('comment', '')) > 0 and len(request.POST.get('comment', '')) <= 320:
            fc = FileComment()
            fc.user = request.user
            fc.file = mfile
            fc.comment = request.POST['comment']
            fc.save()

            # Notifications
            comments = FileComment.objects.filter(file=mfile)
            notified = [request.user]
            for comment in comments:
                if comment.user not in notified:
                    notification = UserNotification()
                    notification.user = comment.user
                    notification.message = _('%s also commented on %s' % (request.user.get_full_name() or request.user.email, mfile.name))
                    notification.link = '/courses/view/%s/%s?file=%s&notify=true' % (mfile.course.id, mfile.parent.id if mfile.parent else '', mfile.id)
                    notification.type = 'filecomment_' + str(mfile.id) + '_' + str(comment.user.id)
                    notification.save()

                    notified.append(comment.user)

            return HttpResponse('Alright.')
        else:
            return HttpResponse(status=500)
    else:
        return HttpResponse(status=500)


@login_required
def createfolder(request):
    if request.method == 'POST':
        if not request.POST['name']:
            return HttpResponse(status=500)

        course = get_object_or_404(Course, members=request.user, id=request.POST['course'])

        parent = None
        if request.POST['parent']:
            parent = get_object_or_404(Folder, course=course, id=request.POST['parent'])

        newfolder = Folder()
        newfolder.creator = request.user
        newfolder.name = request.POST['name']
        newfolder.course = course
        newfolder.parent = parent
        if not newfolder.save():
            messages.error(request, _('There already exists a folder named %s.' % newfolder.name))
        return HttpResponse("Yeah, you rock!")
    else:
        return HttpResponse(status=500)


@login_required
def check_empty(request):
    if request.method == 'POST':
        course = get_object_or_404(Course, members=request.user, id=request.POST['course'])

        if File.objects.filter(parent=request.POST['id'], course=course) or Folder.objects.filter(parent=request.POST['id'], course=course):
            return HttpResponse('Not empty')

        return HttpResponse('Empty. Go ahead.')
    else:
        return HttpResponse(status=500)


@login_required
def deletefolder(request):
    if request.method == 'POST':
        course = get_object_or_404(Course, members=request.user, id=request.POST['course'])

        if File.objects.filter(parent=request.POST['id'], course=course) or Folder.objects.filter(parent=request.POST['id'], course=course):
            return HttpResponse('Not empty')

        folder = get_object_or_404(Folder, id=request.POST['id'], course__members=request.user)
        fname = folder.name
        folder.delete()

        messages.success(request, _('You have successfully deleted the folder %s.' % fname))
        return HttpResponse('Whoops')
    else:
        return HttpResponse(status=500)


@login_required
def deletefile(request):
    if request.method == 'POST':
        file = get_object_or_404(File, course__members=request.user, course=request.POST['course'], owner=request.user, id=request.POST['id'])
        fname = file.name
        file.delete()
        messages.success(request, _('You have successfully deleted the file %s.' % fname))
        return HttpResponse('Okay, my lord.')
    else:
        return HttpResponse(status=500)


@login_required
def send_request(request):
    if request.method == 'POST':
        course = get_object_or_404(Course, id=request.POST['course'], uni=request.user.get_profile().uni)
        course.pending.add(request.user)
        course.save()

        # Notify admins
        for user in course.admins.all():
            notification = UserNotification()
            notification.user = user
            notification.message = _('%s asked to join %s' % (request.user.get_full_name() or request.user.email, course.name))
            notification.link = reverse('courses.views.requests', args=(course.id,))
            notification.type = 'courserequest_' + str(course.id) + '_' + str(request.user.id)
            notification.save()

        return HttpResponse('All done.')
    else:
        return HttpResponse(status=500)


@login_required
def rename_file(request):
    if request.method == 'POST':
        file = get_object_or_404(File, id=request.POST['fileid'], owner=request.user)
        file.name = request.POST['name']
        file.save()
        return HttpResponse('All done.')
    else:
        return HttpResponse(status=500)
