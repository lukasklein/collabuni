from django.conf.urls import patterns, url


urlpatterns = patterns('',
    url(r'^createfolder/$', 'api.views.createfolder'),
    url(r'^deletefolder/$', 'api.views.deletefolder'),
    url(r'^check_empty/$', 'api.views.check_empty'),
    url(r'^deletefile/$', 'api.views.deletefile'),
    url(r'^send_request/$', 'api.views.send_request'),
    url(r'^comments/$', 'api.views.get_comments'),
    url(r'^post_comment/$', 'api.views.post_comment'),
    url(r'^rename_file/$', 'api.views.rename_file'),
)
