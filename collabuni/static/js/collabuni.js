function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    crossDomain: false, // obviates need for sameOrigin test
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type)) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

if (!window.collabuni) {
    window.collabuni = {};
}

window.collabuni.notify = function(tag, message) {
    $('.messages-row').append($('<div/>').addClass('alert').addClass('alert-' + tag).append($('<button/>').addClass('close').attr({type: 'button', 'data-dismiss': 'alert'}).text('x')).append($('<span/>').text(message)).hide().fadeIn());
};

$(function() {
    $('input[type="password"].show')
        .each(function() {
            var that = this;
            $(this).after($('<input>')
                .attr({
                    type: 'checkbox',
                    'class': 'showpw'
                })
                .click(function() {
                    that.type = (that.type === 'password' ? 'text' : 'password');
                })
                .after($('<span>'))
                    .text('Show'));
        });
});