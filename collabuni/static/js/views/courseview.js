$(function () {
    $('.member, .admin').tooltip();

    $('.new-folder').mouseover(function () {
        $(this).removeClass('icon-folder-close');
        $(this).addClass('icon-folder-open');
    }).mouseout(function () {
        $(this).removeClass('icon-folder-open');
        $(this).addClass('icon-folder-close');
    }).click(function () {
        $('.empty-alert').hide();
        $('#newfolder-tr').show();
        $('#newfolder-tr input')
            .val('')
            .focus()
            .unbind('blur')
            .blur(createFolder);
    });

    $('#rlydeletefolder').click(function () {
        if (window.collabuni.folderdeleteid) {
            $.post('/api/deletefolder/',
                {
                    course: window.collabuni.course,
                    id: window.collabuni.folderdeleteid
                },
                function (data) {
                    window.location.reload();
                }
            );
        }
    });

    $('.closetd.folder span').click(function () {
        $('.delfoldername').text($(this).attr('data-name'));
        window.collabuni.folderdeleteid = $(this).attr('data-id');
        $.post('/api/check_empty/',
            {
                course: window.collabuni.course,
                id: $(this).attr('data-id')
            },
            function (data) {
                $('.empty').hide();
                $('.notempty').hide();
                if (data === 'Empty. Go ahead.') {
                    $('.empty').show();
                } else {
                    $('.notempty').show();
                }
                $('#folder-delete-modal').modal();
            }
        );
    });

    $('.closetd.file span.delfile').click(function () {
        $('.delfilename').text($(this).attr('data-name'));
        window.collabuni.filedeleteid = $(this).attr('data-id');
        $('#file-delete-modal').modal();
    });

    $('.closetd.file span.renamefile').click(function () {
        var ael = $('.filedownloadlink', $(this).parent().parent());
        var atd = ael.parent();
        ael.hide();
        if($('.input-append', atd).length === 0) {
            var editel_inp = $('<input type="text">').attr('data-id', ael.attr('data-id')).val(ael.text());
            var editel = $('<div class="input-append">').append(editel_inp).append($('<button class="btn">Save</button>'));
            $('button', editel).click(function () {
                var newname = editel_inp.val();
                $.post('/api/rename_file/', {
                    fileid: editel_inp.attr('data-id'),
                    name: newname
                }, function () {
                    editel.remove();
                    ael.text(newname).show();
                });
            });
            atd.append(editel);
            editel_inp.focus();
        }
    });

    $('#rlydeletefile').click(function () {
        if (window.collabuni.filedeleteid) {
            $.post('/api/deletefile/',
                {
                    course: window.collabuni.course,
                    id: window.collabuni.filedeleteid
                },
                function (data) {
                    window.location.reload();
                }
            );
        }
    });

    
    $('.upload-file').click(function () {
        $('#file-upload-modal').modal();
    });

    $('#upload-file-btn').click(function () {
        var file = $('#id_file').val();
        if (file) {
            $('#file-form').submit();
        } else {
            $('.no-file').show();
        }
    });

    $('.filedownloadlink').each(function(e){
        var type = $(this).attr('data-type');
        var link = $(this).attr('href');
        var id = $(this).attr('data-id');

        if(type === 'image' || type === 'text' || type === 'code' || type === 'pdfdocument') {
            $(this).click(function(e){
                e.preventDefault();
                window.collabuni.fid = id;
                $('#fileviewcontent').empty();
                $('#comments').empty();
                $('#new_comment').val('');
                $('#filedownload').unbind('click');
                $('#filedownload').click(function(){
                    document.location.href = link;
                });
                $('#fileviewheading').text($(this).text());

                // Media things diplay foo

                switch(type) {
                    case 'image':
                        $('#fileviewcontent').append($('<img>').attr('src', link));
                        break;
                    case 'text':
                        $('#fileviewcontent').append($('<div>').attr('id', 'textview').load(link));
                        break;
                    case 'code':
                        $('#fileviewcontent').append($('<pre>').addClass('prettyprint').attr('id', 'codeview'));
                        $.get(link, function(data) {
                            $('#codeview').text(data);
                            prettyPrint();
                        });
                        break;
                    case 'pdfdocument':
                        $('#fileviewcontent').append($('<div id="pdfcontrols"><button id="prev" onclick="window.collabuni.pdf.goPrevious()">Previous</button>&nbsp;<span>Page: <span id="page_num"></span> / <span id="page_count"></span></span>&nbsp;<button id="next" onclick="window.collabuni.pdf.goNext()">Next</button></div>'));
                        $('#fileviewcontent').append($('<canvas>').attr('id', 'pdfcanvas'));
                        window.collabuni.pdf = displayPdf(link);
                        break;
                }

                // Comments
                new EJS({url: window.collabuni.static_url + 'templates/comments.ejs'}).update('comments','/api/comments/?file=' + window.collabuni.fid);
                window.setTimeout(function(){$('.timeago').timeago();}, 1000);

                $('#file-view-modal').modal();
                return false;
            });
        } else {
            $(this).click(function(e){
                alert(123);
                document.location.href = link;
            });
        }
    });

    $('#comment_form').submit(function(e) {
        e.preventDefault();
        $.post('/api/post_comment/?file=' + window.collabuni.fid, {
            'comment': $('#new_comment').val()
        }, function() {
            new EJS({url: window.collabuni.static_url + 'templates/comments.ejs'}).update('comments','/api/comments/?file=' + window.collabuni.fid);
            window.setTimeout(function(){$('.timeago').timeago();}, 1000);
            $('#new_comment').val('');
        });
        return false;
    });

    $('#new_comment').keyup(function() {
        var length = $(this).val().length;
        if(320-length < 0) {
            $('#post_comment').attr('disabled', 'disabled');
        } else {
            $('#post_comment').removeAttr('disabled');
        }
        $("#char_remaining").text(320-length);
    });
});

var createFolder = function () {
    $(this).unbind('blur');

    $('#newfolder-tr').hide();
    $('#newfolder-waiting').show();

    var foldername = $('#newfolder-tr input').val();

    if (!foldername) {
        var prevtext = $('#newfolder-waiting td').text();
        $('#newfolder-waiting td').text(window.collabuni.text_empty_foldername);
        $('#newfolder-waiting').removeClass('info').addClass('error');
        window.setTimeout(function () {
            $('#newfolder-waiting').fadeOut(function () {
                $('#newfolder-waiting td').text(prevtext);
                $('#newfolder-waiting').removeClass('error').addClass('info');
            });
        }, 2000);
    } else {
        $.post('/api/createfolder/',
            {
                parent: window.collabuni.parent,
                course: window.collabuni.course,
                name: foldername
            },
            function (data) {
                window.setTimeout(function(){window.location.reload();}, 500);
            }
        );
    }
};

var displayPdf = function(url) {
    PDFJS.disableWorker = true;

    var pdfDoc = null,
        pageNum = 1,
        scale = 0.8,
        canvas = document.getElementById('pdfcanvas'),
        ctx = canvas.getContext('2d');

    //
    // Get page info from document, resize canvas accordingly, and render page
    //
    function renderPage(num) {
      // Using promise to fetch the page
      pdfDoc.getPage(num).then(function(page) {
        var viewport = page.getViewport(scale);
        canvas.height = viewport.height;
        canvas.width = viewport.width;

        // Render PDF page into canvas context
        var renderContext = {
          canvasContext: ctx,
          viewport: viewport
        };
        page.render(renderContext);
      });

      // Update page counters
      document.getElementById('page_num').textContent = pageNum;
      document.getElementById('page_count').textContent = pdfDoc.numPages;
    }

    //
    // Go to previous page
    //
    function goPrevious() {
      if (pageNum <= 1)
        return;
      pageNum--;
      renderPage(pageNum);
    }

    //
    // Go to next page
    //
    function goNext() {
      if (pageNum >= pdfDoc.numPages)
        return;
      pageNum++;
      renderPage(pageNum);
    }

    //
    // Asynchronously download PDF as an ArrayBuffer
    //
    PDFJS.getDocument(url).then(function getPdfHelloWorld(_pdfDoc) {
      pdfDoc = _pdfDoc;
      renderPage(pageNum);
    });

    return {
        goNext: goNext,
        goPrevious: goPrevious
    };
};