$(function () {
    $('.request-course').click(function () {
        if ($(this).hasClass('disabled')) {
            return;
        }
        var cid = $(this).attr('data-id');
        window.collabuni.requested = this;
        $.post('/api/send_request/',
            {
                course: cid
            },
            function (data) {
                $(window.collabuni.requested).addClass('disabled').parent().parent().addClass('warning');
                window.collabuni.requested = null;
                window.collabuni.notify('success', window.collabuni.text_request_success);
            }
        );
    });
});