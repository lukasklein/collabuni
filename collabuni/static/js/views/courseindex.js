$(function () {
    $('.latest-files tr').each(function () {
        $(this).click(function () {
            var folderid = $(this).attr('data-folder');
            var courseid = $(this).attr('data-course');
            var fileid = $(this).attr('data-id');
            var folderstring = '';
            if (folderid) {
                folderstring = folderid + '/';
            }
            document.location = '/courses/view/' + courseid + '/' + folderstring + '?file=' + fileid + '&notify=true';
        });
    });

    var max_height = Math.max.apply(null, $('.course-well').map(function () {
        return $(this).height();
    }).get());
    
    $('.course-well').height(max_height);
});