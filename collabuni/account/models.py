from django.db import models


class WaitingList(models.Model):
    email = models.EmailField()
    created = models.DateTimeField(auto_now_add=True)
