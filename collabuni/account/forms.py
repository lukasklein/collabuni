from django import forms
from django.utils.translation import ugettext as _


class AccountDetails(forms.Form):
    fname = forms.CharField(max_length=255, label=_('First Name'))
    lname = forms.CharField(max_length=255, label=_('Last Name'))
    email = forms.EmailField(label=_('E-Mail'))
    unimail = forms.EmailField(widget=forms.TextInput(attrs={'disabled': 'disabled'}), label=_('Uni E-Mail'), help_text=_('While you are able to change your primary email, your uni email is required to link you to your uni.'), required=False)
    uni = forms.CharField(max_length=255, widget=forms.TextInput(attrs={'disabled': 'disabled'}), label=_('University'), required=False)


class JoinForm(forms.Form):
    email = forms.EmailField()


class PasswordForm(forms.Form):
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
        'password_incorrect': _('Your old password was entered incorrectly. '
                                'Please enter it again.'),
    }

    password_old = forms.CharField(max_length=255, label=_('Current Password'), widget=forms.PasswordInput())
    password_new = forms.CharField(max_length=255, label=_('New Password'), widget=forms.PasswordInput(attrs={'class': 'show'}))
    password_new_c = forms.CharField(max_length=255, label=_('New Password (repeat)'), widget=forms.PasswordInput(attrs={'class': 'show'}))

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(PasswordForm, self).__init__(*args, **kwargs)

    def clean_password_new_c(self):
        password1 = self.cleaned_data.get('password_new')
        password2 = self.cleaned_data.get('password_new_c')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'])
        return password2

    def clean_password_old(self):
        """
        Validates that the old_password field is correct.
        """
        old_password = self.cleaned_data["password_old"]
        if not self.user.check_password(old_password):
            raise forms.ValidationError(
                self.error_messages['password_incorrect'])
        return old_password

    def save(self, commit=True):
        self.user.set_password(self.cleaned_data['password_new'])
        if commit:
            self.user.save()
        return self.user
