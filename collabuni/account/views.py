from random import choice
import string
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.contrib.auth import logout
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth import login as login_o
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from courses.models import Course
from unis.models import Uni
from account.models import WaitingList
from account.forms import AccountDetails, PasswordForm, JoinForm


@login_required
def index(request):
    courses = Course.objects.filter(members=request.user)

    tab = 'general'

    initial_data = {
        'email': request.user.email,
        'fname': request.user.first_name,
        'lname': request.user.last_name,
        'unimail': request.user.get_profile().uni_mail,
        'uni': request.user.get_profile().uni,
    }

    form1 = AccountDetails(initial=initial_data)
    form2 = PasswordForm(user=request.user)

    if request.method == 'POST':
        if request.POST.get('form1'):
            tab = 'general'
            data = request.POST.copy()
            data['unimail'] = request.user.get_profile().uni_mail
            data['uni'] = request.user.get_profile().uni
            form1 = AccountDetails(data)
            if form1.is_valid():
                request.user.first_name = form1.cleaned_data['fname']
                request.user.last_name = form1.cleaned_data['lname']
                request.user.email = form1.cleaned_data['email']
                request.user.save()
                messages.success(request, _('Your details are now up to date.'))
        elif request.POST.get('form2'):
            tab = 'password'
            form2 = PasswordForm(user=request.user, data=request.POST)
            if form2.is_valid():
                form2.save()
                messages.success(request, _('You have successfully changed your password.'))

    return render_to_response('index_account.html', {
        'courses': courses,
        'headline': [_('My Account'), reverse('account.views.index')],
        'form1': form1,
        'form2': form2,
        'tab': tab,
    }, context_instance=RequestContext(request))


@login_required
def vlogout(request):
    logout(request)
    return HttpResponseRedirect('/')


def thanks(request):
    return render_to_response('thanks.html', {
            'headline': [_('Thanks!'), reverse('account.views.thanks')],
    }, context_instance=RequestContext(request))


def login(request):
    return login_o(request, request.user)


def join(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('courses.views.index'))

    unis = Uni.objects.all()

    errors = ''
    email = ''
    uni_does_not_exist = False
    maillist_success = False
    if request.method == "POST":
        form = JoinForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            try:
                User.objects.get(email=email)
                errors = _('A user with this email already exists.')
            except ObjectDoesNotExist:
                try:
                    Uni.objects.get(mails__domain=email.split('@')[1])
                    username = ''.join([choice(string.letters) for i in xrange(30)])
                    chars = string.letters + string.digits
                    password = ''.join(choice(chars) for i in xrange(10))
                    user = User.objects.create_user(username, email, password)
                    user.save()

                    subject, from_mail, to = _('Welcome to Collabuni'), 'collabuni@productgang.com', email
                    text = _('Hey, thanks for signing up at Collabuni.\nWasn\'t you? No worries, just ignore this mail.\nOtherwise, here\'s your password for collabuni.com:\n%s\n\nPlease remember to change it in the account settings as soon as possible!\n\nYour\'s sincerely,\nLukas from the Product Gang' % password)
                    send_mail(subject, text, from_mail, [to], fail_silently=True)
                    return HttpResponseRedirect(reverse('account.views.thanks'))
                except ObjectDoesNotExist:
                    uni_does_not_exist = True

                if request.POST.get('notify'):
                    uni_does_not_exist = False
                    maillist_success = True
                    from postmonkey import PostMonkey
                    pm = PostMonkey('8e28da74fe1fdab1bfa7fc4f1653ddae-us6')
                    pm.listSubscribe(id='a604576888', email_address=email)
                    wl = WaitingList()
                    wl.email = email
                    wl.save()
        else:
            errors = form.errors['email']

    return render_to_response('join.html', {
        'headline': [_('Join Collabuni'), reverse('account.views.join')],
        'errors': errors,
        'uni_does_not_exist': uni_does_not_exist,
        'unis': unis,
        'email': email,
        'maillist_success': maillist_success,
    }, context_instance=RequestContext(request))
