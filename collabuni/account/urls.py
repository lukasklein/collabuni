from django.conf.urls import patterns, url


urlpatterns = patterns('',
    url(r'^$', 'account.views.index'),
    url(r'^logout/$', 'account.views.vlogout'),
)
